variable "people" {
	type = list(object({
		name = string
		age = optional(number, 0)
	}))
}

locals {
	sorted_names = distinct(sort(var.people[*].name))
	people_sorted_by_name = [for name in reverse(local.sorted_names) : [for person in var.people : (person.age > 0 ? join(" ", [person.name, person.age]) : person.name) if person.name == name]]
	people_over_30 = [for person in var.people : person.name if person.age >= 30]
}


output "sorted_by_name" {
    value = yamlencode(local.people_sorted_by_name)
}

output "over_30" {
    value = yamlencode(local.people_over_30)
}