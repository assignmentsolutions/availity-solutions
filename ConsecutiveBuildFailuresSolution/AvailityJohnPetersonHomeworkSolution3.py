from operator import contains
import datetime

class JobInfo:
    """JobInfo stores a job's name, the build number, and the result of the job"""
    
    def __init__(self, JOB_NAME, BUILD_NUMBER, RESULT):
        self.JOB_NAME = JOB_NAME
        self.BUILD_NUMBER = BUILD_NUMBER
        self.RESULT = RESULT


JobList = []
NameList = []
DataFile = open("data.txt", "r")

for line in DataFile:
    #I want to make sure I don't capture any empty lines and don't throw newlines onto the RESULT variables
    tempLine = line.replace("\n", "")
    if len(tempLine) != 0: JobList.append(JobInfo(tempLine.split(' ')[0], int(tempLine.split(' ')[1]), tempLine.split(' ')[2]))

JobList.sort(key=lambda x: x.BUILD_NUMBER)

#First going to run through the jobs to make sure all unique job names are added to the NameList
for job in JobList:
    if not contains(NameList, job.JOB_NAME): NameList.append(job.JOB_NAME)

#Now I am going to run through the JobList for each name in NameList to find the max consecutive instance of each job name failing    
for name in NameList:
    MaxCount = 0
    CurrentCount = 0
    for job in JobList:
        if job.JOB_NAME != name: continue
        
        if job.RESULT == "FAILED": CurrentCount += 1
        elif CurrentCount > MaxCount:
            MaxCount = CurrentCount
            CurrentCount = 0
        else: CurrentCount = 0
    if CurrentCount > MaxCount: MaxCount = CurrentCount

    print(name + " had " + str(MaxCount) + " consecutive build failures.")